import java.time.LocalDateTime;

public class MonitoredData {

	private LocalDateTime start_time;
	private LocalDateTime end_time;
	private String activity_label;
	
	
	
	public MonitoredData(LocalDateTime start_time, LocalDateTime end_time, String activity_label) {
		super();
		this.start_time = start_time;
		this.end_time = end_time;
		this.activity_label = activity_label;
	}



	public LocalDateTime getStart_time() {
		return start_time;
	}




	public void setStart_time(LocalDateTime start_time) {
		this.start_time = start_time;
	}




	public LocalDateTime getEnd_time() {
		return end_time;
	}




	public void setEnd_time(LocalDateTime end_time) {
		this.end_time = end_time;
	}




	public String getActivity_label() {
		return activity_label;
	}




	public void setActivity_label(String activity_label) {
		this.activity_label = activity_label;
	}




	@Override
	public String toString() {
		return "start_time-->   " + start_time.toString() + "  , end_time-->  " + end_time.toString() + "  ,  activity_label = "
				+ activity_label;
	}
	

}
