import static java.lang.Math.toIntExact;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Tasks {
	private static final DateTimeFormatter parser = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	//private static final DateTimeFormatter parser = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
	private String file_name_activities;
	public Tasks(String file_name) {
		super();
		this.file_name_activities = file_name;
		// TODO Auto-generated constructor stub
	}


	public List<MonitoredData> ReadActivitiesIntoList(String file_name){
		//String file_name="Activities.txt";
		
		File file = new File(file_name);
		Stream<String> stream = null;
		try {
			stream = Files.lines(file.toPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//final DateTimeFormatter parser = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		
		List<MonitoredData> activities = new ArrayList<MonitoredData>();
		
		stream.forEach(line -> {
			String[] parts = line.split("\t\t");
			activities.add(new MonitoredData(LocalDateTime.parse(parts[0],parser), LocalDateTime.parse(parts[1],parser), parts[2].trim()));
			
			});
	
		/*for(MonitoredData e:activities) {
			System.out.println(e.toString());
		}*/
		
		return activities;
	}
	
	public void WriteinFile(String file, String toWrite) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.append(toWrite);
			writer.append("\n");
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public File CreateFileforTask(String name_file) {
		File file = new File(name_file);
		
		return file;
	}
	
	void TASK_1() {
		File task1 = new File("Task_1.txt");
		List<MonitoredData> activities = ReadActivitiesIntoList(file_name_activities);
		
		StringBuilder data = new StringBuilder();
		//activities.stream().forEach(activity -> System.out.println(activity.getEnd_time().) );
		
		activities.stream().forEach(activity -> data.append(activity.toString() + "\n") );
		//System.out.println(data);
		
		WriteinFile("Task_1.txt",String.valueOf(data));
		
	}
	
	void TASK_2() {
		File task1 = new File("Task_2.txt");
		List<MonitoredData> activities = ReadActivitiesIntoList(file_name_activities);
		
		long nr  =(long) activities.stream().map(activity -> activity.getStart_time().getDayOfMonth() ).distinct().count();
		
		//System.out.println("Task2: "+nr);
		
		StringBuilder data = new StringBuilder();
		data.append("Number of distinct days: "+nr);
		WriteinFile("Task_2.txt",String.valueOf(data));
	}
	
	void TASK_3() {
		File task1 = new File("Task_3.txt");
		List<MonitoredData> activities = ReadActivitiesIntoList(file_name_activities);
		
		Map<String, Long> freq;
		freq = activities.stream().map(activity -> activity.getActivity_label()).collect(Collectors.groupingBy(
                Function.identity(), Collectors.counting()
        ));
		
		StringBuilder data = new StringBuilder();
		
		for (Entry<String, Long> e : freq.entrySet())  
            data.append(e.getKey() + " = " + e.getValue() + "\n");
		//System.out.println(data);
		
		WriteinFile("Task_3.txt",String.valueOf(data));
	}
	
	void TASK_4() {
		File task1 = new File("Task_4.txt");
		List<MonitoredData> activities = ReadActivitiesIntoList(file_name_activities);
		
		Map<Integer, Map<String,Long>> freq_per_day;
	
		freq_per_day = activities.stream().collect(Collectors.groupingBy (
                      e -> e.getStart_time().getDayOfMonth(),
                      Collectors.groupingBy(MonitoredData::getActivity_label,
                                            Collectors.counting())));
		
		StringBuilder data = new StringBuilder();
		for (Map.Entry<Integer, Map<String,Long>> e : freq_per_day.entrySet()) {
			data.append("Day :" + e.getKey() + "\n");
            Map<String, Long> aux= e.getValue();
            for(Map.Entry<String, Long> ee: aux.entrySet()) {
            	data.append("\tActivity: "+ ee.getKey() + ", number of times: "+ee.getValue() + "\n");
            }
		}
		//System.out.println(data);
		WriteinFile("Task_4.txt",String.valueOf(data));
		
	}
	
	public LocalTime calculate_duration(String activity_name) {
		List<MonitoredData> activities = ReadActivitiesIntoList(file_name_activities);

		
		
		Long hours = 0L;
		Long minutes = 0L;
		Long seconds = 0L;
		
		LocalTime sum = LocalTime.of(0, 0, 0);
		
		for(MonitoredData e:activities) {
			if(e.getActivity_label().equals(activity_name)) {
				//hours += ChronoUnit.HOURS.between(e.getStart_time(), e.getEnd_time()); 
				//minutes += ChronoUnit.MINUTES.between(e.getStart_time(), e.getEnd_time()); 
				//seconds += ChronoUnit.SECONDS.between(e.getStart_time(), e.getEnd_time()); 
				LocalTime t = LocalTime.of(0, 0, 0); 
						t = t.plusHours(e.getStart_time().getHour());
						t = t.plusMinutes(e.getStart_time().getMinute());
						t = t.plusSeconds(e.getStart_time().getSecond());
						
				LocalTime t1 = LocalTime.of(0, 0, 0); 
						t1 = t1.plusHours(e.getEnd_time().getHour());
						t1 = t1.plusMinutes(e.getEnd_time().getMinute());
						t1 = t1.plusSeconds(e.getEnd_time().getSecond());	
				//System.out.println(t.getHour() + ":"+ t.getMinute() + ":"+t.getSecond() +" -> " + t1.getHour() + ":"+ t1.getMinute() + ":"+t1.getSecond());		
				
				t1 = t1.minusSeconds(t.getSecond());
				t1 = t1.minusMinutes(t.getMinute());
				t1 = t1.minusHours(t.getHour());
				
				sum = sum.plusSeconds(t1.getSecond());
				sum = sum.plusMinutes(t1.getMinute());
				sum = sum.plusHours(t1.getHour());
					
				//System.out.println(sum.getHour() + ":" + sum.getMinute() + ":" + sum.getSecond());	
				
				//hours += e.getEnd_time().getHour() - e.getStart_time().getHour();
				//minutes += e.getEnd_time().getMinute() - e.getStart_time().getMinute();
				//seconds += e.getEnd_time().getSecond() - e.getStart_time().getSecond();
				//System.out.println(hours + ":" + minutes + ":" + seconds );
				//sum.plusHours( (Long) e.getEnd_time().minusHours(e.getStart_time().getHour()) );
				//sum.with(e.getEnd_time().minusMinutes(e.getStart_time().getMinute()));
				//sum.with(e.getEnd_time().minusMinutes(e.getStart_time().getMinute()));
			}
		
		}
		
		int hours1 = toIntExact(hours);
		int minutes1 = toIntExact(minutes);
		int seconds1 = toIntExact(seconds);
		/*while(seconds1>60) {
			seconds1 = seconds1 - 60;
			minutes1 = minutes1 + 1;
			
		}
		while(minutes1>60) {
			minutes1 = minutes1 - 60;
			hours1 = hours1 + 1;
			
		}*/
		
		//System.out.println(hours1 + ":" + minutes1 + ":" +seconds1);
		return sum;
		
	}
	
	void TASK_5() {
		File task1 = new File("Task_5.txt");
		List<MonitoredData> activities = ReadActivitiesIntoList(file_name_activities);
		//Duration d = new Duration();
		
		//Map<String, LocalTime> activieties_duration = activities.stream().map(MonitoredData::getActivity_label).collect(Collectors.groupingBy(
               // Function.identity(), calculate_duration() ));
		
		//Map<String, LocalTime> activieties_duration = activities.stream().collect(Collectors.toMap(
			//	activity -> activity.getActivity_label() , (activity) -> calculate_duration( activity.getActivity_label() )
	            
	      //  ));
		
		//Map<String, LocalTime> activieties_duration1 = activities.stream().collect(Collectors.groupingBy (a -> a.getActivity_label(), new Collector(0,0,0) ));
		
	       // Map<String, LocalTime> activieties_duration = null;
        //));
		//LocalTime t = calculate_duration("Leaving");
		//System.out.println(t);
		
		Map<String, Long> freq;
		freq = activities.stream().map(activity -> activity.getActivity_label()).collect(Collectors.groupingBy(
                Function.identity(), Collectors.counting()
        ));
		
		Map<String, LocalTime> activieties_duration = null;
		
		StringBuilder data = new StringBuilder();
		
		for(Map.Entry<String, Long> ee: freq.entrySet()) {
        	//data.append("\tActivity: "+ ee.getKey() + ", number of times: "+ee.getValue() + "\n");
			LocalTime t = calculate_duration(ee.getKey() );
			data.append("\tActivity: "+ ee.getKey() + ", duration: " + t + "\n");
			//String activity = ee.getKey();
			//if(t!=null)
				//activieties_duration.put(activity, t  );
        }		
		
		
		//for(Map.Entry<String, LocalTime> ee: activieties_duration.entrySet()) {
        	//data.append("\tActivity: "+ ee.getKey() + ", number of times: "+ee.getValue() + "\n");
			//System.out.println("\tActivity: "+ ee.getKey() + ", duration: "+ee.getValue() + "\n");
        //}
		//System.out.println(data);
		WriteinFile("Task_5.txt",String.valueOf(data));
	}
	
	void TASK_6() {
		File task1 = new File("Task_6.txt");
		List<MonitoredData> activities = ReadActivitiesIntoList(file_name_activities);
		
		//List<String> short_activities = activities.stream().collect(collector)
	}

}
